package com.example.android.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView textView1;
    private TextView textView2;
    ViewModel viewModel = new ViewModel();
    String answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView1 = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);

    }

    public void numberEvent(View view){

        String number = textView2.getText().toString();

        switch (view.getId())
        {
            case R.id.button0:
                number += "0";
                break;
            case R.id.button1:
                number += "1";
                break;
            case R.id.button2:
                number += "2";
                break;
            case R.id.button3:
                number += "3";
                break;
            case R.id.button4:
                number += "4";
                break;
            case R.id.button5:
                number += "5";
                break;
            case R.id.button6:
                number += "6";
                break;
            case R.id.button7:
                number += "7";
                break;
            case R.id.button8:
                number += "8";
                break;
            case R.id.button9:
                number += "9";
                break;
            case R.id.button00:
                number += "00";
                break;
            case R.id.buttonplus:
                if(!number.isEmpty()){
                char c1 = number.charAt(number.length()-1);
                if(Character.isDigit(c1)){
                    number += " + ";
                }}
                break;
            case R.id.buttonminus:
                if(!number.isEmpty()){
                    char c2 = number.charAt(number.length()-1);
                if(Character.isDigit(c2)){
                    number += " - ";
                }}
                break;
            case R.id.buttonmultiple:
                if(!number.isEmpty()){
                char c3 = number.charAt(number.length()-1);
                if(Character.isDigit(c3)){
                    number += " * ";
                }}
                break;
            case R.id.buttondivide:
                if(!number.isEmpty()){
                    char c4 = number.charAt(number.length()-1);
                if(Character.isDigit(c4)){
                    number += " / ";
                }}
                break;
            case R.id.buttonpercentage:
                number += "%";
                break;
            case R.id.buttondot:
                if(!number.isEmpty()){
                    if(!number.contains(".")){
                    number += ".";
                }}
                break;
            case R.id.buttoncancle:
                if(number.length()>0) {
                    if(number.charAt(number.length()-1)==' ')
                        number = number.substring(0, number.length() - 3);
                    else
                        number = number.substring(0, number.length() - 1);
                }else if(number.length()==0)
                {
                    answer="0";
                }
                break;
            case R.id.buttonAC:
                number = "";
                answer = "";
                break;
            case R.id.buttonequal:
                if(number.length()==0){
                    Toast.makeText(MainActivity.this,"Empty Equation",Toast.LENGTH_SHORT).show();
                }
                else if(Character.isDigit(number.charAt(number.length()-1)))
                {
                    answer = viewModel.equal(number);
                    break;
                }
                else
                {
                    Toast.makeText(MainActivity.this,"Invalid Equation",Toast.LENGTH_SHORT).show();
                }

        }
        textView2.setText(number);
        textView1.setText(answer);
    }

}